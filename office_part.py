#!/Users/sunxuanzhi/anaconda3/bin/python3
# -*- coding:utf-8 -*-
#author:xuanzhi
import datetime
import time
from pprint import pprint
import collections
from read_csv import Read_csv

class Stadium_Booking():
	def __init__(self):
		self.info = {'A':{},'B':{},'C':{},'D':{}}
		self._BOOKING_TM = ['09'] + [str(i) for i in range(10,23)]
		self._dates = {'A':[],'B':[],'C':[],'D':[]}
		self._fees = {'A':{},'B':{},'C':{},'D':{}}

	def _func_1(self):
		_BOOK_TM = {}
		for i in range(9,22):
			_BOOK_TM.update({i:''})
		return _BOOK_TM

	def _earning(self):
		_total = 0
		print('> 收入汇总\n> ---')
		for _bk_plc,_ff in self._ordered_dict().items():
			_temp_total = 0
			print('> 场地: ',_bk_plc)
			#print(_ff)
			for item in _ff:
				if item[-1][0] != 0:
					_temp_total += item[-1][0]
					if item[-1][2] != 0:
						print('> ',item[0],' ',str(item[-1][0]),'元',' 已优惠:',str(item[-1][2]),'元')
					else:
						print('> ',item[0],' ',str(item[-1][0]),'元')
				else:
					_temp_total += item[-1][1]
					print('> ',item[0],' 违约金 ',str(item[-1][1]),'元')
			_total += _temp_total
			print('> 小计: ',str(_temp_total),'元')
			print('>')
		print('> ---\n> 总计:  ',str(_total),'元')

	def _ordered_dict(self):
		_raw_fees = {}
		_ordered_list = []
		self._ordered_fees = {}
		for i,j in self._fees.items():
			_raw_fees = {}
			_raw_fees_tm = {}
			_min_ts = 0
			_single_list = []
			for m,n in j.items():
				_booking_dt, _booking_tm = m.split(' ')[0], m.split(' ')[1]
				_start_tm = _booking_tm.split('~')[0].split(':')[0]
				_dtime = datetime.datetime.strptime(_booking_dt + _start_tm.split(':')[0],'%Y-%m-%d%H')
				_unix_tm = int(time.mktime(_dtime.timetuple()))
				_raw_fees_tm.update({_unix_tm:m})
				_raw_fees.update({_unix_tm:n})
			#print(_raw_fees_tm)
			for key in sorted(_raw_fees.keys()):
					_single_list.append([_raw_fees_tm[key],_raw_fees[key]])				
			self._ordered_fees.update({i:_single_list})
		return self._ordered_fees

	def _check_discount(self):
		_discount = Read_csv()._trans_to_ts()
		#print(_discount)
		return _discount




	def _check_input(self,_input_str):
		if _input_str == '收入汇总':
			self._earning()
			return None
		_input_list = _input_str.strip().split(' ')
		#print(_input_list)
		try:
			if _input_list[3] not in self.info.keys():
				return []
			if not self._check_time(_input_list[1]+' '+_input_list[2])[0]:
				#print(_input_list[1]+' '+_input_list[2])
				return []
			return _input_list
		except:
			return []


	def _get_input(self,_input):
		_input_list = self._check_input(_input)
		#print(_input_list)
		if _input_list == None:
			return None
		if len(_input_list) == 4:
			_user_name, _booking_time, _booking_place  = _input_list[0], _input_list[1]+' '+_input_list[2], _input_list[3]
			#print(_user_name, _booking_time, _booking_place)
			_status = self._check_status(_booking_place, _booking_time, _user_name)
			if _status == 'free':
				print('> Success: the booking is accepted!')
			elif _status == 'bad_input':
				print('> Error: the booking is invalid!')
			elif _status == 'has_been_booked':
				print('> Error: the booking conflicts with existing bookings!')
			#print('--------------------------')
		elif len(_input_list) == 5 and _input_list[4] == 'C':
			_user_name, _booking_time, _booking_place, _cancel  = _input_list[0], _input_list[1]+' '+_input_list[2], _input_list[3], _input_list[4]
			#print(_booking_place, _booking_time, _user_name, _cancel)
			_status = self._check_status(_booking_place, _booking_time, _user_name, _cancel)
			if _status == 'can_be_canceled':
				print('> Success: the booking is accepted!')
			elif _status == 'bad_input':
				print('> Error: the booking is invalid!')
			elif _status == 'free':
				print('> Error: the booking being cancelled does not exist!')
			#print('--------------------------')
		else:
			print('> Error: the booking is invalid!')
			#print('--------------------------')


	def _check_status(self,_booking_place, _booking_time, _user_name, _cancel=None ):
		#print(_user_name, _booking_time, _booking_place,_cancel)
		if _booking_place in self.info.keys() and self._check_time(_booking_time)[0]:
			_booking_hours = [i for i in range(int(_booking_time.split(' ')[1].split('~')[0].split(':')[0]),int(_booking_time.split(' ')[1].split('~')[1].split(':')[0]))]
			#print(_booking_hours)
			if _cancel == None:
				_temp_ts = int(time.time())
				#print(self._dates)
				if _booking_time.split(' ')[0] not in self._dates[_booking_place]:
					self._dates[_booking_place].append(_booking_time.split(' ')[0])
					self.info[_booking_place][_booking_time.split(' ')[0]] = self._func_1()
					for hour in _booking_hours:
						if self.info[_booking_place][_booking_time.split(' ')[0]][hour] == '':
							self.info[_booking_place][_booking_time.split(' ')[0]][hour] = [_user_name,_temp_ts]
					#pprint(self.info)
					_booking_once_fee = self._week_fees(self._check_time(_booking_time)[1],_booking_hours) * self._booking_start_unix(_booking_time)//10
					_discount_fee = self._week_fees(self._check_time(_booking_time)[1],_booking_hours) - _booking_once_fee
					self._fees[_booking_place][_booking_time] = [_booking_once_fee,0,_discount_fee]
					return 'free'
				else:
					for hour in _booking_hours:
						if self.info[_booking_place][_booking_time.split(' ')[0]][hour] != '':
							return 'has_been_booked'
					for hour in _booking_hours: 
						self.info[_booking_place][_booking_time.split(' ')[0]][hour] = [_user_name,_temp_ts]
					_booking_once_fee = self._week_fees(self._check_time(_booking_time)[1],_booking_hours) * self._booking_start_unix(_booking_time)//10
					_discount_fee = self._week_fees(self._check_time(_booking_time)[1],_booking_hours) - _booking_once_fee
					self._fees[_booking_place][_booking_time] = [_booking_once_fee,0,_discount_fee]
					return 'free'
			else:
				if _booking_time.split(' ')[0] not in self._dates[_booking_place]:
					return 'free'
				for hour in _booking_hours:
					if self.info[_booking_place][_booking_time.split(' ')[0]][hour][0] != _user_name:
						return 'free'
				_ts = self.info[_booking_place][_booking_time.split(' ')[0]][hour][1]
				for hour in _booking_hours:
					if self.info[_booking_place][_booking_time.split(' ')[0]][hour][1] != _ts:
						return 'free'
					else:
						self.info[_booking_place][_booking_time.split(' ')[0]][hour] = ''
				if self.info[_booking_place][_booking_time.split(' ')[0]] == self._func_1():
					self._dates[_booking_place].remove(_booking_time.split(' ')[0])
				_original_fee = self._fees[_booking_place][_booking_time][0]
				if self._check_time(_booking_time)[1] in [k for k in range(5)]:
					self._fees[_booking_place][_booking_time] = [0,int(_original_fee*0.5),0]
				else:
					self._fees[_booking_place][_booking_time] = [0,int(_original_fee*0.25),0]
				return 'can_be_canceled'
		else:
			return 'bad_input'

	def _week_fees(self,_week_day,_booking_hours):
		_fee = 0
		#print(_booking_hours)
		if _week_day in [k for k in range(5)]:
			for i in _booking_hours:
				i = str(i)
				if i == '9':
					i = '09'
				if i in self._BOOKING_TM[:3]:
					_fee += 30
				elif i in self._BOOKING_TM[3:9]:
					_fee += 50
				elif i in self._BOOKING_TM[9:11]:
					_fee += 80
				else:
					_fee += 60
		else:
			for i in _booking_hours:
				i = str(i)
				if i == '9':
					i = '09'
				if i in self._BOOKING_TM[:3]:
					_fee += 40
				elif i in self._BOOKING_TM[3:9]:
					_fee += 50
				else:
					_fee += 60
		return _fee





	#检查预定时间是否合法
	def _check_time(self, _booking_time):
		_booking_dt, _booking_tm = _booking_time.split(' ')[0], _booking_time.split(' ')[1]
		#print(_booking_dt, _booking_tm)
		try:
			_booking_datetime = datetime.datetime.strptime(_booking_dt,'%Y-%m-%d')
			week_day = _booking_datetime.weekday()
		except:
			return False
		try:
			#print(_booking_datetime,type(_booking_datetime))
			_start_tm, _end_tm = _booking_tm.split('~')[0], _booking_tm.split('~')[1]
			#print(_start_tm, _end_tm)

			if _start_tm[-2:] != '00' or _end_tm[-2:] != '00':
				return False	
			elif (_start_tm.split(':')[0] not in self._BOOKING_TM) or (_end_tm.split(':')[0] not in self._BOOKING_TM) or (int(_start_tm.split(':')[0]) >= int(_end_tm.split(':')[0])) :
				return False
			else:
				return True, week_day
		except:
			return False

	def _booking_start_unix(self, _booking_time):
		if self._check_time(_booking_time)[0]:
			_dis_end = datetime.datetime.strptime(_booking_time.split(' ')[0],'%Y-%m-%d') + datetime.timedelta(days=1)
			_dis_end_unix = int(time.mktime(_dis_end.timetuple()))
			for k,v in self._check_discount().items():
				if int(k.split('_')[0]) <= _dis_end_unix <= int(k.split('_')[1]):
					return v
		return 10






if __name__ == '__main__':
	client = Stadium_Booking()	
	#test_1
	#client._get_input('abcdefghijklmnopqrst1234567890')
	#client._get_input('U001 2016-06-02 22:00~22:00 A')
	#client._get_input('U002 2017-08-01 19:00~22:00 A')
	#client._get_input('U003 2017-08-02 13:00~17:00 B')
	#client._get_input('U004 2017-08-03 15:00~16:00 C')
	#client._get_input('U005 2017-08-05 09:00~11:00 D')
	#test_2
	#client._get_input('U002 2017-08-01 19:00~22:00 A')
	#client._get_input('U003 2017-08-01 18:00~20:00 A')
	#client._get_input('U002 2017-08-01 19:00~22:00 A C')
	#client._get_input('U002 2017-08-01 19:00~22:00 A C')
	#client._get_input('U003 2017-08-01 18:00~20:00 A')
	#client._get_input('U003 2017-08-02 13:00~17:00 B')
	#pprint(client.info)
	#print(client._dates)
	#print(client._fees)
	#print(client._ordered_dict())
	#client._earning()
	while True:
		_keyboard_input = input()
		client._get_input(_keyboard_input)








