import csv
import datetime
import time

file_path = 'discount.csv'

class Read_csv(object):
	def __init__(self):
		self._discount = dict()


	def _read_discount(self):
		with open(file_path, 'r', encoding = 'utf-8-sig') as input_file:
			filereader = csv.reader(input_file,delimiter=' ')
			rows = [row for row in filereader]
		return rows


	def _trans_to_ts(self):
		for i in self._read_discount():
			_dis_start = datetime.datetime.strptime(i[0],'%Y-%m-%d')
			#print(type(_dis_start))
			_dis_start_unix = int(time.mktime(_dis_start.timetuple()))

			_dis_end = datetime.datetime.strptime(i[1],'%Y-%m-%d') + datetime.timedelta(days=1)
			_dis_end_unix = int(time.mktime(_dis_end.timetuple()))
			_dis = int(i[2])
			self._discount.update({str(_dis_start_unix)+'_'+str(_dis_end_unix):_dis})
		return self._discount

if __name__ == '__main__':
	csv = Read_csv()._trans_to_ts()
	print(csv)